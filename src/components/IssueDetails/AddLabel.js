import React, { Component } from "react";
import { Menu, MenuItem, Button, Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";

import DownArrow from "@material-ui/icons/ArrowDropDown";

const styles = theme => ({
  searchbar: {
    backgroundColor: "silver",
    border: "1px solid gray",
    boxShadow: "1.5px"
  },
  gap: {
    marginLeft: "0.8rem"
  },
  iconColor: {
    color: "black",
    marginLeft: "0.2rem"
  },
  container: {
    paddingLeft: "12rem"
  }
});
class AddLabel extends Component {
  state = {
    anchorEl: null
  };
  handleProfileMenuOpen = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleMenuClose = (labelName, id) => {
    this.setState({ anchorEl: null });
    this.props.addLabels(labelName, id)
  };

  render() {
    const { classes } = this.props;
    const { anchorEl } = this.state;
    const isMenuOpen = Boolean(anchorEl);
    
    const listItem = this.props.labels.map((label, index) => (
      <MenuItem key={index} onClick={() => this.handleMenuClose(label, index)}>
        {label}
      </MenuItem>
    ));

    const dropDownMenu = (
      <Menu
        anchorEl={anchorEl}
        anchorOrigin={{ vertical: "top", horizontal: "right" }}
        transformOrigin={{ vertical: "top", horizontal: "right" }}
        open={isMenuOpen}
        // onClose={this.handleMenuClose}
      >
        {listItem}
      </Menu>
    );

    const buttons = (
      <Button
        onClick={this.handleProfileMenuOpen}
        className={classes.gap}
      >
        <Typography className="" variant="h6" color="inherit" noWrap />
        Add Label
        <DownArrow />
      </Button>
    );
    return (
      <div>
        {buttons}
        {dropDownMenu}
      </div>
    );
  }
}
export default withStyles(styles)(AddLabel);
