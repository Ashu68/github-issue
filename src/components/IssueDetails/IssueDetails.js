import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { Paper, Typography, Button } from "@material-ui/core";
import ErrorIcon from "@material-ui/icons/ErrorOutline";
import CircularProgress from "@material-ui/core/CircularProgress";
import "bootstrap/dist/css/bootstrap.min.css";

import AddLabel from "./AddLabel";
import Comments from "./Comments";
import Labels from "./Labels";
import AddComments from "./AddComments";

const styles = theme => ({
  card: {
    marginRight: "3rem",
    marginLeft: "1rem",
    paddingLeft: "1rem",
    paddingRight: "1rem",
    width: "40rem",
    height: "fitContext"
  },
  cardContainer: {
    marginTop: "2rem",
    marginLeft: "3rem",
    marginRight: "3rem",
    display: "flex",
    justifyContent: "spaceBetween"
  },
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2
  },
   progress: {
    margin: theme.spacing.unit * 2
  },
  progressBar: {
    display: "flex",
    justifyContent: "center",
    marginTop: "1rem"
  },
  issueId: {
    color: "gray"
  },
  stateButton: {
    color: "white",
    display: "inlineBlock",
    width: "5rem",
    height: "2rem",
    backgroundColor: "green",
    border: "1px solid gray"
  }
});

class IssueDetails extends Component {
  state = {
    issue: [],
    user: null,
    comments: [],
    issueLabels: [],
    allLabels: [],
    state: null,
    commentId: null,
    loadingIssue: false
  };

  handleExpandClick = () => {
    this.setState(state => ({ expanded: !state.expanded }));
  };

  componentDidMount() {
    this.getIssueDetails();
    this.getLabels();
  }

  getIssueDetails = () => {
    const url = `https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues/${
      this.props.match.params.id
    }`;
    fetch(url, {
      headers: {
        Authorization: "token bd8f7732f4ebad7211e870a889c8747969945bf3"
      }
    })
      .then(resp => resp.json())
      .then(result => {
        this.setState({
          issue: result,
          state: result.state,
          user: result.user.login,
          issueLabels: result.labels
        });
      })
      .then(() => this.getComments())
      .catch(e => console.error(e));
      this.setState({
        loadingIssue: true
      })
  };

  getComments = () => {
    const url = `https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues/${
      this.state.issue.number
    }/comments`;
    fetch(url, {
      headers: {
        Authorization: "token bd8f7732f4ebad7211e870a889c8747969945bf3"
      }
    })
      .then(resp => resp.json())
      .then(result => {
        this.setState({
          comments: result
        });
      })
      .catch(e => console.error(e));
  };

  getLabels = () => {
    const url = `https://api.github.com/repos/freeCodeCamp/freeCodeCamp/labels`;
    fetch(url, {
      headers: {
        Authorization: "token bd8f7732f4ebad7211e870a889c8747969945bf3"
      }
    })
      .then(resp => resp.json())
      .then(result => {
        this.setState({
          allLabels: result
        });
      })
      .then(() => this.getUniqueLabels())
      .catch(e => console.error(e));
  };

  postComment = comment => {
    const url = `https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues/${this.state.issue.number}/comments`;
    fetch(url, {
      method: "POST",
      headers: {
        Authorization: "token bd8f7732f4ebad7211e870a889c8747969945bf3"
      },
      body: JSON.stringify({
        body: comment
      })
    })
      .then(response => response.json())
      .then(result => {
        this.setState({
          commentId: result.id
        })
        return result.body
      })
      .then((body) => this.addComment(body))
      .catch(e => console.error(e));
  }
  addComment = (comment) => {
    let newComment = this.state.comments;
    newComment = newComment.concat({
      user: {
        login: "Aashu",
        avatar_url: "https://avatars3.githubusercontent.com/u/26092756?v=4"
      },
      id: this.state.commentId,
      body: comment
    });
    this.setState({
      comments: newComment
    });
  }
    

  deleteComment = (id, index) => {
    const url = `https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues/comments/${id}`;
    fetch(url, {
      method: "DELETE",
      headers: {
        Authorization: "token bd8f7732f4ebad7211e870a889c8747969945bf3"
      },
    })
    let newComment = this.state.comments;
    newComment.splice(index, 1);
    this.setState({
      comments: newComment
    });
  };

  getUniqueLabels = () => {
    let labels = this.state.allLabels;
    labels = [
      ...new Set(
        labels.reduce((acc, label) => {
          acc.push(label.name);
          return acc;
        }, [])
      )
    ];
    this.setState({
      allLabels: labels
    });
  };

  addLabels = (labelName, id) => {
    if (labelName && id) {
      let newLabel = this.state.issueLabels;
      newLabel = newLabel.concat({
        name: labelName
      });
      let labels = this.state.allLabels;
      labels.splice(id, 1);
      this.setState({
        issueLabels: newLabel,
        allLabels: labels
      });
    }
  };

  deleteLabels = id => {
    let newLabel = this.state.issueLabels;
    let allLabels = this.state.allLabels;
    let deletedLabel = newLabel.splice(id, 1);
    console.log(deletedLabel)
    if (!allLabels.includes(deletedLabel)) {
      allLabels = allLabels.concat(deletedLabel[0].name)
      this.setState({
        issueLabels: newLabel,
        allLabels: allLabels
      });
    }
    else {
      this.setState({
        issueLabels: newLabel
      });
    }
  };

  handleState = () => {
    if (this.state.state === "open")
      this.setState({
        state: "close"
      });
    else
      this.setState(
        {
          state: "open"
        }
      );
  }

  render() {
    const comments = this.state.comments.map((comment, index) => (
      <Comments
        key={index}
        id={index}
        deleteComment={this.deleteComment}
        commentCard={comment}
        labels={this.state.labels}
      />
    ));
    const { classes } = this.props;

    return (
      !this.state.loadingIssue ? (
        <div className={classes.progressBar}>
          <CircularProgress className={classes.progress} />
        </div>
      ) :
        (
          <div>
          <Paper className={classes.root} elevation={1}>
            <Typography variant="h5" component="h3">
              {this.state.issue.title}
              <span className={classes.issueId}>#{this.state.issue.number}</span>
            </Typography>
            <Typography>
              <Button className={classes.stateButton} onClick={this.handleState}>
                <ErrorIcon />
                {this.state.state}
              </Button>
              {this.state.user}
            </Typography>
            <AddLabel labels={this.state.allLabels} addLabels={this.addLabels} />
            <Labels
              labels={this.state.issueLabels}
              deleteLabels={this.deleteLabels}
            />
          </Paper>
          {comments}
          <AddComments postComment={this.postComment} />
      </div>
        )
  // }
    );
  }
}

export default withStyles(styles)(IssueDetails);
