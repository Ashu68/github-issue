import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import {
  Card,
  AppBar,
  Button,
  TextField
} from "@material-ui/core";

const styles = theme => ({
  container: {
    display: "flex",
    flexDirection: "column"
  },
  textField: {
    width: "20rem",
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  cardContainer: {
    height: "5rem",
    marginTop: "2rem",
    marginLeft: "6rem",
    marginRight: "4rem"
  },
  card: {
    width: "40rem",
    marginRight: "3rem",
    marginLeft: "1rem"
  },
  addButton: {
    width: "3rem",
    backgroundColor: "green",
    color: "white",
    height: "2rem",
    textAlign: "center"
  }
});

class AddComments extends Component {
  state = {
    comment: ""
  };

  addComment = () => {
    this.props.postComment(this.state.comment)
    this.setState({
      comment: ""
    })
  };

  handleChange = (event) => {
    this.setState({ 
      comment: event.target.value
    });
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.cardContainer}>
        <Card className={classes.card}>
          <AppBar position="static" color="default">
            Add a Comment
          </AppBar>
          <form className={classes.container} noValidate autoComplete="off">
            <TextField
              id="outlined-bare"
              className={classes.textField}
              defaultValue={this.state.comment}
              onChange={this.handleChange}
              margin="normal"
              variant="outlined"
            />
            <Button
              onClick={this.addComment}
              className={classes.addButton}
            >
              Add
            </Button>
          </form>
        </Card>
      </div>
    );
  }
}

export default withStyles(styles)(AddComments);
