import React from "react"
import { withStyles } from "@material-ui/core/styles";
import Chip from "@material-ui/core/Chip";


const styles = theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
  },
  chip: {
    margin: theme.spacing.unit,
  },
});

const Labels = props => {
  const handleDelete = id => {
      props.deleteLabels(id);
    };
    const { classes } = props
    
    const labels = props.labels.map((label, index) => (
      <Chip
        key={index}
        label={label.name}
        onDelete={() => handleDelete(index)}
        style={{background:`#${label.color}`}}
        className={classes.chip}
        color="primary"
      />
    ));

    return <div className={classes.root}>{labels}</div>;
}

export default withStyles(styles)( Labels)