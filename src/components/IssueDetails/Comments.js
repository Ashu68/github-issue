import React from "react";
import {
  Card,
  CardContent,
  Typography
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  card: {
    marginRight: "3rem",
    marginLeft: "1rem",
    paddingLeft: "1rem",
    paddingRight: "1rem",
    width: "40rem",
    height: "fitContext"
  },
  cardImage: {
    width: "3rem",
    height: "3rem",
    borderRadius: "0"
  },
  cardContainer: {
    marginTop: "2rem",
    marginLeft: "3rem",
    marginRight: "3rem",
    display: "flex",
    justifyContent: "spaceBetween"
  },
});

const Comments = props => {
  const { classes } = props;
  const deleteComment = (id, index) => {
    // console.log(id, index)
    props.deleteComment(id, index)
  }

    return (
      <div className={classes.cardContainer}>
        <img
          src={props.commentCard.user.avatar_url}
          className={classes.cardImage}
          alt="user avatar"
        />
        <Card className={classes.card}>
          <Typography>{props.commentCard.user.login}</Typography>
          <CardContent>{props.commentCard.body}</CardContent>
          <button onClick={() => deleteComment(props.commentCard.id, props.id)}> Delete </button>
        </Card>
      </div>
    );
};

export default withStyles(styles)(Comments);
