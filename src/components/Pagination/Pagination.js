import React from "react";
import { Button } from "@material-ui/core";
import {withStyles} from "@material-ui/core/styles"
import "bootstrap/dist/css/bootstrap.min.css";

const styles = theme => ({
    button: {
        backgroundColor: "blue",
    }
})

const Pagination = props => {
    const nextPage = () => {
        props.handleNextPage();
    }
    const prevoiusPage = () => {
        props.handlePreviousPage()
    }
    const {classes} = props
    return (
        <div>
            <Button onClick={prevoiusPage} className={classes.button} > prev </Button>
            <Button className={classes.button} > {props.activePage} </Button>
            <Button onClick={nextPage} className={classes.button} > next </Button>
        </div>
    );
};

export default withStyles(styles)(Pagination);
