import React, { Component } from "react"
import { Menu, MenuItem, Button, Typography } from "@material-ui/core"
import { withStyles } from "@material-ui/core/styles"

import DownArrow from "@material-ui/icons/ArrowDropDown";

const styles = theme => ({
  searchbar: {
    backgroundColor: "silver",
    border: "1px solid gray",
    boxShadow: "1.5px"
  },
  gap: {
    marginLeft: "0.8rem"
  },
  iconColor: {
    color: "black",
    marginLeft: "0.2rem"
  },
  container: {
    paddingLeft: "12rem"
  }
});
class DropDownMenu extends Component {
    state = {
        anchorEl: null
    }
    handleProfileMenuOpen = event => {
        this.setState({ anchorEl: event.currentTarget });
    };

    handleMenuClose = e => {
        this.setState({ anchorEl: null });
        this.props.issueFunctions(e.target.id, e.target.innerText, this.props.buttonText);
    };
    
    render() {
        const {classes} = this.props
        const { anchorEl } = this.state
        const isMenuOpen = Boolean(anchorEl);
        
        const listItem = this.props.items.map((item, index) => (
            <MenuItem onClick={this.handleMenuClose} id={index + 1}>
                {item}
            </MenuItem>
        ))

        const dropDownMenu = (
            <Menu
                anchorEl={anchorEl}
                anchorOrigin={{ vertical: "top", horizontal: "right" }}
                transformOrigin={{ vertical: "top", horizontal: "right" }}
                open={isMenuOpen}
                onClose={this.handleMenuClose}
            >
                {listItem}
            </Menu>
        )
            
        const buttons = (
            <Button
                onClick={this.handleProfileMenuOpen}
                id={this.props.buttonText}
                className={classes.gap}
            >
            <Typography className="" variant="h6" color="inherit" noWrap />
                {this.props.buttonText}
                <DownArrow />
            </Button>
        )
    return (
      <div>
            {buttons}
            {dropDownMenu}
      </div>
    );
    }
}
export default withStyles (styles)(DropDownMenu)