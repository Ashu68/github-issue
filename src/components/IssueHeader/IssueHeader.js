import React from "react";
import { AppBar, Toolbar, Button, Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import ErrorIcon from "@material-ui/icons/ErrorOutline";
import DoneIcon from "@material-ui/icons/Done";

import DropDownMenu from "./DropDownMenu";

const styles = theme => ({
  searchbar: {
    backgroundColor: "silver",
    border: "1px solid gray",
    boxShadow: "1.5px"
  },
  gap: {
    marginLeft: "0.8rem"
  },
  iconColor: {
    color: "black",
    marginLeft: "0.2rem"
  },
  container: {
    paddingLeft: "12rem"
  }
});

const IssueHeader = props => {
  
  const handleIssuesState = state => {
    props.handleState(state);
  };

  const issueFunctions = (id, name, buttonText) => {
    console.log(id, name, buttonText);
    
    props.getFilteredIssues(id, buttonText, name)
  };
  
  const listItems = [
    { buttonText: "Author", items: props.authorsName },
    { buttonText: "Labels", items: props.labelsName },
    {
      buttonText: "Sort",
      items: ["Newest", "Oldest", "Most commented", "Least commented"]
    }
  ];
  
  const { classes } = props;
  
  const dropDownMenu = listItems.map((item, index) => (
    <DropDownMenu
      id={index + 1}
      buttonText={item.buttonText}
      items={item.items}
      issueFunctions={issueFunctions}
    />
  ));
  
  return (
    <div>
      <AppBar position="static" className={classes.searchbar}>
        <Toolbar>
          <Button
            className={classes.iconColor}
            value="open"
            id="1"
            onClick={() => handleIssuesState("open")}
          >
            <ErrorIcon />
            <Typography>{"Opened"}</Typography>
          </Button>

          <Button
            className={classes.iconColor}
            value="close"
            id="2"
            onClick={() => handleIssuesState("close")}
          >
            <DoneIcon />
            <Typography>{"Closed"}</Typography>
          </Button>
          {dropDownMenu}
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default withStyles(styles)(IssueHeader);
