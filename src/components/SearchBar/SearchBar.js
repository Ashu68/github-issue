import React from "react";
import { InputBase, IconButton } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import { withStyles } from "@material-ui/core/styles";
import { fade } from "@material-ui/core/styles/colorManipulator";

const styles = theme => ({
  search: {
    position: "relative",
    marginBottom: "1rem",
    border: "1px solid gray",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25)
    },
    marginLeft: 0,
    width: "15rem",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing.unit,
      width: "20rem"
    }
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: "100%",
    position: "absolute",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  grow: {
    flexGrow: 1
  },
  inputRoot: {
    height: "3rem",
    color: "inherit",
    width: "20rem"
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create("width"),
    width: "20rem",
    [theme.breakpoints.up("sm")]: {
      width: 120,
      "&:focus": {
        width: 200
      }
    }
  }
});

const SearchBar = props => {
  const handleSearchIssue = event => {
    props.searchIssueBy(event.target.value);
  };
  const { classes } = props;
  return (
    <div>
      <div className={classes.search}>
        <div className={classes.searchIcon}>
          <IconButton>
            <SearchIcon />
          </IconButton>
        </div>
        <InputBase
          placeholder="is:issue is:open"
          onChange={handleSearchIssue}
          classes={{
            root: classes.inputRoot,
            input: classes.inputInput
          }}
        />
      </div>
    </div>
  );
};

export default withStyles(styles)(SearchBar);
