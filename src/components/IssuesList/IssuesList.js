import React from "react";
import { withStyles } from "@material-ui/core/styles";
import {
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  Typography,
  Button
} from "@material-ui/core";
import ErrorIcon from "@material-ui/icons/ErrorOutline";
import CommentIcon from "@material-ui/icons/ChatBubbleOutline";
import Moment from "moment";
import { Link } from "react-router-dom";

const styles = theme => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper,
    borderBottom: "1px solid gray"
  },
  errorIcon: {
    color: "green"
  },
  button: {
    width: "0rem"
  },
  inline: {
    display: "inline"
  },
  title: {
    color: "black",
    textDecoration: "none",
    cursor: "pointer"
  }
});

const IssuesList = props => {
  const time = new Date().toISOString();

  const { classes } = props;
  const updatedDate = Moment(props.issue.updated_at);
  const currentDate = Moment(time);
  const duration = updatedDate.from(currentDate);

  return (
    <div key={props.id}>
      <List className={classes.root}>
        <ListItem alignItems="flex-start">
          <ListItemIcon>
            <ErrorIcon className={classes.errorIcon} />
          </ListItemIcon>
          <ListItemText
            primary={
              <React.Fragment>
                <Link
                  to={`/${props.issue.number}`}
                  className={classes.title}
                >
                  <Typography />
                  {props.issue.title}
                </Link>
              </React.Fragment>
            }
            secondary={
              <React.Fragment>
                <Typography
                  component="span"
                  className={classes.inline}
                  color="textPrimary"
                />
                {`#${props.issue.number} opened ${duration} by ${
                  props.issue.user.login
                }`}
              </React.Fragment>
            }
          />
          <Button className={classes.button}>
            <CommentIcon />
            <Typography
              component="span"
              className={classes.inline}
              color="textPrimary"
            />
            {props.issue.comments}
          </Button>
        </ListItem>
      </List>
    </div>
  );
};

export default withStyles(styles)(IssuesList);
