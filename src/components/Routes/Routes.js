import React from 'react'
import {BrowserRouter, Switch, Route} from "react-router-dom"

import App from "../../App"
import IssueDetails from "../IssueDetails/IssueDetails"

const Routes = props => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" component={App} exact={true} />
        <Route path="/:id" component={IssueDetails} />
      </Switch>
    </BrowserRouter>
  );
}

export default Routes
