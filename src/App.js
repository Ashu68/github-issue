import React, { Component } from "react";
import "./App.css";
import { withStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import Fuse from "fuse.js"

import "bootstrap/dist/css/bootstrap.min.css";

import Pagination from "./components/Pagination/Pagination";
import SearchBar from "./components/SearchBar/SearchBar";
import IssueHeader from "./components/IssueHeader/IssueHeader";
import IssuesList from "./components/IssuesList/IssuesList";

const styles = theme => ({
  progress: {
    margin: theme.spacing.unit * 2
  },
  progressBar: {
    display: "flex",
    justifyContent: "center",
    marginTop: "1rem"
  }
});
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      issues: [],
      filteredIssues: [],
      labelsName: [],
      authorsName: [],
      issuesBySort: [],
      issuesByAuthor: [],
      issuesByLabel: [],
      issuesByState: [],
      filteredSort: 0,
      filteredAuthor: 0,
      filteredLabel: 0,
      filteredState: null,
      loading: false,
      activePage: 1
    };
  }

  componentDidMount() {
    this.getIssuesPerPage();
    let options = {
      keys: ["title"],
      id: "id"
    };
    this.fuse = new Fuse(this.state.issues, options);
  }

  getIssuesPerPage = () => {
    let url = `https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues?page=${
      this.state.activePage
    }&per_page=25`;
    fetch(url, {
      headers: {
        Authorization: "token bd8f7732f4ebad7211e870a889c8747969945bf3"
      }
    })
      .then(resp => resp.json())
      .then(result => {
        this.setState({
          filteredIssues: result,
          issuesBySort: result,
          issuesByLabel: result,
          issuesByAuthor: result,
          issuesByState: result,
          issues: result,
          loading: true
        });
      })
      .then(() => {
        this.getLabelsName();
        this.getAuthorsName();
      })
      .catch(err => console.error(err));
  };

  getLabelsName = () => {
    let issues = [...this.state.issues];
    let tempIssues = [];
    let newIssues = issues.forEach(issue => {
      if (issue.labels.length !== 0)
        issue.labels.forEach(label => {
          tempIssues.push(label.name);
        });
    });
    newIssues = [...new Set(tempIssues)];
    this.setState({
      labelsName: newIssues
    });
  };

  getAuthorsName = () => {
    let issues = this.state.issues;
    let newIssues = issues.reduce((acc, issue) => {
      if (!acc.includes(issue.user.login)) acc = acc.concat(issue.user.login);
      return acc;
    }, []);
    this.setState({
      authorsName: newIssues
    });
  };

  searchIssueBy = title => {
    let issues = this.state.issues;
    if (title) {
      const searched = this.fuse.search(title);
      issues = issues.filter(post => searched.includes(post.id.toString()));
      this.setState({
        issues: issues
      });
    }
  };

  handleState = state => {
    let currentState = state === "open" ? "open" : "close";
    let issues = this.state.issues;
    let newIssues = issues.filter(issue => issue.state === currentState);
    this.setState({
      filteredState: currentState,
      issuesByState: newIssues
    });
  };

  sortedIssue = (id, issuesBySort) => {
    let issues = issuesBySort
    let newIssues = issues;
    switch (id) {
      case "1":
        newIssues = newIssues.sort((a, b) =>
          a.created_at > b.created_at ? 1 : -1
        );
        break;
      case "2":
        newIssues = newIssues.sort((a, b) =>
          a.created_at < b.created_at ? 1 : -1
        );
        break;
      case "3":
        newIssues = newIssues.sort((a, b) =>
          a.comments < b.comments ? 1 : -1
        );
        break;
      case "4":
        newIssues = newIssues.sort((a, b) =>
          a.comments > b.comments ? 1 : -1
        );
        break;
      default:
        newIssues = issues;
    }
    return newIssues
  };

  filterByAuthors = (name, issuesByAuthor) => {
    
    let issues = issuesByAuthor;
    if (name) {
      
      let newIssues = issues.filter(issue => issue.user.login === name);
      return newIssues;
    }
  }

  filterByLabels = (name, issuesByLabel) => {
    let issues = issuesByLabel;
    let issueList = [];
    if (name) {
      let newIssues = issues.forEach(issue => {
        if (issue.labels.length !== 0)
          issue.labels.forEach(label => {
            if (label.name === name) issueList.push(issue);
          });
      });
      return issueList
    }
  };

  getFilteredIssues = (id, buttonName, itemName) => {
    console.log(id, buttonName, itemName)
    const {
      issues,
      issuesBySort,
      issuesByLabel,
      issuesByAuthor,
      // issuesByState,
      filteredSort,
      filteredAuthor,
      filteredLabel,
      filteredState
    } = this.state;

    let filteredIssues;
    // let newissues = issues;
    switch (buttonName) {
      case "Sort":
        filteredIssues = this.sortedIssue(id, issuesBySort);
        
        let isArrEqual = issuesBySort.every(function(element, index) {
          return element === issues[index];
        });
        if (isArrEqual) {
          console.log("here");

          this.setState(
            {
              filteredAuthor: 0,
              filteredLabel: 0,
              filteredState: null
            },
            () => {
              this.setState({
                issuesByAuthor: filteredIssues,
                issuesByLabel: filteredIssues,
                issuesByState: filteredIssues
              });
            }
          );
        }
        
        if (filteredAuthor === 0) {
          this.setState({
            issuesByAuthor: filteredIssues
          });
        }
        if (filteredLabel === 0) {
          this.setState({
            issuesByLabel: filteredIssues
          });
        }
        if (filteredState === null) {
          this.setState({
            issuesByState: filteredIssues
          });
        }
        this.setState({
            filteredSort: 1
          });
        break;
      case "Author":
        filteredIssues = this.filterByAuthors(itemName, issuesByAuthor)
        isArrEqual = issuesByAuthor.every(function(element, index) {
         return element === issues[index];
       });
       if (isArrEqual) {
         console.log("here");

         this.setState(
           {
             filteredAuthor: 0,
             filteredLabel: 0,
             filteredState: null
           },
           () => {
             this.setState({
               issuesBySort: filteredIssues,
               issuesByLabel: filteredIssues,
               issuesByState: filteredIssues
             });
           }
         );
       }
        if (filteredSort === 0) {
          this.setState({
            issuesBySort: filteredIssues
          });
        }
        if (filteredLabel === 0) {
          this.setState({
            issuesByLabel: filteredIssues
          });
        }
        if (filteredState === null) {
          this.setState({
            issuesByState: filteredIssues
          });
        }
        this.setState(
          {
            filteredAuthor: 1,
          });
        break;
      case "Labels":
        filteredIssues = this.filterByLabels(itemName, issuesByLabel)
        // console.log(issuesByLabel)
        
        isArrEqual = issuesByLabel.every(function(element, index) {
          return element === issues[index];
        });
        if (isArrEqual) {
          console.log("here");

          this.setState(
            {
              filteredAuthor: 0,
              filteredLabel: 0,
              filteredState: null
            },
            () => {
              this.setState({
                issuesBySort: filteredIssues,
                issuesByAuthor: filteredIssues,
                issuesByState: filteredIssues
              });
            }
          );
        }
        
        if (filteredAuthor === 0) {
          this.setState({
            issuesByAuthor: this.state.issuesByLabel
          });
        }
        if (filteredState === null) {
          this.setState({
            issuesByState: this.state.issuesByLabel
          });
        }
        if (filteredSort === 0) {
          this.setState({
            issuesBySort: this.state.issuesByLabel
          });
        }
        this.setState(
          {
            filteredLabel: 1,
          });
        break;
      default:
        this.setState({
            issues: issues
        })
    }
    this.setState({
      issues: filteredIssues
    });
  };

  handlePreviousPage = () => {
    let pageNumber = this.state.activePage;
    if (pageNumber > 1) {
      this.setState(
        {
          activePage: pageNumber - 1
        },
        () => this.getIssuesPerPage()
      );
    }
  };

  handleNextPage = () => {
    let pageNumber = this.state.activePage;
    if (pageNumber < 10) {
      this.setState(
        {
          activePage: pageNumber + 1
        },
        () => this.getIssuesPerPage()
      );
    }
  };

  render() {
    const { classes } = this.props;
    const issueList = this.state.issues.map(issue => (
      <IssuesList key={issue.id} issue={issue} handleId={this.handleId} />
    ));
    return (
      <div className="app">
        <SearchBar searchIssueBy={this.searchIssueBy} />
        <IssueHeader
          sortedIssue={this.sortedIssue}
          filterByLabels={this.filterByLabels}
          filterByAuthor={this.filterByAuthor}
          handleState={this.handleState}
          labelsName={this.state.labelsName}
          authorsName={this.state.authorsName}
          menuItems={this.menuItems}
          getFilteredIssues={this.getFilteredIssues}
        />
        {!this.state.loading ? (
          <div className={classes.progressBar}>
            <CircularProgress className={classes.progress} />
          </div>
        ) : (
          <div>
            {" "}
            {issueList}
            <div className="d-flex justify-content-center">
              <Pagination
                handleNextPage={this.handleNextPage}
                handlePreviousPage={this.handlePreviousPage}
                activePage={this.state.activePage}
              />
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default withStyles(styles)(App);
